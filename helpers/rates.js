const axios = require('axios');
const dateHelper = require('./date');
const env = require('dotenv').config();
const BASE_API_URL = process.env.BASE_URL;



module.exports = {
    getHistoricalExchangeRates: async (daysCount,currencies) => {
        let results = [];
        const currencySymbols = currencies.join(",");
        
        let dates = dateHelper.lastDates(daysCount);
        if (Array.isArray(dates)) {
            //Loop through dates
            dates.reverse();
            for(const date of dates ){
                results[date] = [];
                //console.log(BASE_API_URL + date);
                try {
                    let dateData = await axios({
                        method:"GET",
                        url : BASE_API_URL + date,
                        headers: {
                            "content-type":"application/x-www-form-urlencoded"
                        },
                        params: {
                            access_key : process.env.AUTH_TOKEN,
                            symbols : currencySymbols,
                            format : 1

                        }
                    })
                    let gbpRate = dateData.data.rates.GBP;
                    for (const [key, value] of Object.entries(dateData.data.rates)) {
                        results[date][key] = (value/gbpRate).toFixed(6);
                    }
                } catch(e) {
                    console.log(e.response.error);
                }
            }
        }else{
            throw new Error('Dates not set properly!');
        }
        return results;
    }
}
