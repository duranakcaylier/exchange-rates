module.exports = {
    lastDates: function(dayCount) {
        var dates = [];
        for (var i=0; i<dayCount; i++) {
            var d = new Date();
            d.setDate(d.getDate() - i);
            dates.push( module.exports.formatDate(d) )
        }
        return(dates);
    },
    formatDate: function(date) {
        var dd = date.getDate();
        var mm = date.getMonth()+1;
        var yyyy = date.getFullYear();
        if(dd<10) {dd='0'+dd}
        if(mm<10) {mm='0'+mm}
        date = yyyy + '-' + mm + '-' + dd;
        return date
    },
};